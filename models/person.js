//Sample Mongoose Schema (Person class)
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
//    ObjectId = Schema.ObjectId;

var personSchema = new Schema({
//    id: ObjectId,
    name: String,
    lastName: String
});

//Export the schema
module.exports = mongoose.model('Person', personSchema);
